# Jeu D&D Java 

Jeu de sytle Donjon et Dragon codé en Java

Doc : https://drive.google.com/drive/folders/1ci2Bz6GtkkSmWoA4AQwYpR-L18tMASms?usp=sharing


## 📚 Auteurs

👤 **S. NOURY**

👤 **E. HOUNZANGBE**

👤 **I. LAURIANO**


## 💻 Installation et dépendances

Ce projet utilise:
OpenJDK 11




## 🚀 Utilisation



## 📝 License

Copyright © 2020-2021 S. NOURY, E. HOUNZANGBE, I. LAURIANO

This project is GPL licensed.

<img src="https://stri-online.net/FTLV/pluginfile.php/1/theme_adaptable/adaptablemarketingimages/0/Logos.png" width="50%" alt="banner_upssitech"/>
