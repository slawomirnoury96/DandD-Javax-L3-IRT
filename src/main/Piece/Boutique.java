package main.Piece;

import main.Equipement.*;
public class Boutique extends Piece{

    protected Arme arme;
    protected Armure armure;
    protected Potion potion;

    public Boutique(){
        this.arme = new Arme();
        this.armure = new Armure();
        this.potion = new Potion();
    }
    /**@return Affiche l'inventaire du marchant */
    public String getInventaire(){
        String text = "Le marchant à\n";
        text = text +"Une arme de "+ this.arme.getBonus()+" de dégat\n";
        text = text +"Une armure de "+ this.armure.getBonus()+" de défense\n";
        text = text +"Une potion de "+ this.armure.getBonus()+" de soin\n";
        return text;
    }

    public Arme getArme(){ 
        Arme tampon = this.arme; 
        this.arme = new Arme();
        return tampon;
    }

    public Armure getArmure(){ 
        Armure tampon = this.armure; 
        this.armure = new Armure();
        return tampon;
    }

    public Potion getPotion(){ 
        Potion tampon = this.potion; 
        this.potion = new Potion();
        return tampon;
    }

    public Integer getPrix(){ //Donne le prix
        return 2;
    }

}
