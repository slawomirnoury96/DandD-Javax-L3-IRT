package main.Piece;

import main.EtreVivant.Joueur;
import main.EtreVivant.Monstre;

import java.util.concurrent.ThreadLocalRandom;

import main.Equipement.*;
/** Les pièces/environnement de combat */
public class Combat extends Piece{
    Monstre groupe[]; //Groupe de monstre
    public Combat(){
        //Rajouter un variable pour des combat multi monstre
        groupe = new Monstre[1];
        groupe[0] = new Monstre();

    }

    public Monstre[] GetMonstre(){
        return groupe;
    }

    public Boolean IsMonstre(){
        
        for (int i = 0; i < groupe.length; i++){
            if(groupe[i]!=null){
                return true;
            }
        }
        return false;
    }

    public String InCombat(Joueur joueur){

        
        //Pour li'tégration multi-monstre, ajourter des véfication null sur les monstres mort avant l'attaque
        this.groupe[0].subirAtk(joueur.attaquer());
        if(this.groupe[0].getPointDeVie()<=0){ //On check si le monstre est mort, si oui code en dessous
            this.groupe[0] = null;
            if(this.IsMonstre()){
                //On a encore des monstre
            }else{
                System.out.println("Les monstres sont morts \n");
                //On fournis une récompense
                switch(ThreadLocalRandom.current().nextInt(1, 4 + 1)){
                    case 1: //Epée
                        joueur.addInventaire(new Arme());
                        break;
                    case 2: //Armure
                        joueur.addInventaire(new Armure());
                        break;
                    case 3: //Argent
                        joueur.addArgent(ThreadLocalRandom.current().nextInt(1, 10 + 1));
                        break;
                    case 4: //Potion
                        joueur.addInventaire(new Potion());
                        break;
                    
                }
                return "win";
            }
        }
        joueur.subirAtk(this.groupe[0].attaquer());
        if(joueur.getPointDeVie()<=0){ //On check si le joueur est mort, si oui code en dessous
            System.out.println("Le joueur est mort \n");
            return "lose";
        } 
        return "draw";
    }
}

