package main.Labyrinthe;


import main.DataType.Point;
import main.Piece.*;

public class Labyrinthe {
    protected Piece carte[][]; 
    protected Point taille; //Taille du labyrinthe
    public Labyrinthe(){ //Initalisation de la carte, statique pour l'instant
        this.taille = new Point();
        this.taille.x =8;
        this.taille.y = 8;
        this.carte=new Piece[this.taille.x][this.taille.y];
        this.carte[6][3]=new Piece();
        this.carte[5][3]=new Piece();
        this.carte[4][3]=new Piece();
        this.carte[5][4]=new Piece();
        this.carte[5][5]=new Piece();
        this.carte[7][3]=new Boutique();
        this.carte[5][2]=new Combat();
        this.carte[4][5]=new Combat();
        
    }


    public Point getBoutiquePoint(){
        int i,j;
        Point point = new Point();
        for (i = 0; i < this.carte.length; i++) {
            for (j = 0; j < this.carte[i].length; j++) {
                if (this.carte[i][j] instanceof Boutique){
                    point.x = i;
                    point.y = j;
                    return point;
                }
            }
        }
        return null;
    }

    public String getPieceString(Point point){//debug Array
        String type = "null";
        if (this.carte[point.x][point.y] instanceof Piece){
            type = "piece";
        }
        if (this.carte[point.x][point.y] instanceof Boutique){
            type = "boutique";
        }
        if (this.carte[point.x][point.y] instanceof Combat){
            type = "combat";
        }
        return type;
    }

    public Boolean isPiece(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                if (this.carte[point.x][point.y] instanceof Piece){
                    return true;
                }
            }
        }
            return false;
    }

    public Boolean isCombat(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                if (this.carte[point.x][point.y] instanceof Combat){
                    return true;
                }
            }
        }
            return false;
    }

    public Boolean isBoutique(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                if (this.carte[point.x][point.y] instanceof Boutique){
                    return true;
                }
            }
        }
            return false;
    }

    public Combat getCombat(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                if (this.carte[point.x][point.y] instanceof Combat){
                    Combat combat = (Combat) this.carte[point.x][point.y];
                    return combat;
                }
            }
        }
            return null;
    }

    public Boutique getBoutique(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                if (this.carte[point.x][point.y] instanceof Boutique){
                    Boutique boutique = (Boutique) this.carte[point.x][point.y];
                    return boutique;
                }
            }
        }
            return null;
    }

    public Boolean setPiece(Point point){
        if(point != null){
            if((0<=point.x && taille.x>point.x)&&(0<=point.y && taille.y>point.y)){ // On vérifie si on est dans les limite de la carte 
                this.carte[point.x][point.y] = new Piece();
                return true;
            }
        }
        return false;
    }

    public String getPorte(Point point){
        String text = "Les portes de la piéces sont : " ;
        Point target = new Point();
        target.x = point.x;
        target.y = point.y;

        target.x = target.x - 1;
        if(this.isPiece(target)){
            text = text + " H ";
        }
        target.x = target.x + 2;
        if(this.isPiece(target)){
            text = text + " B ";
        }
        target.x = point.x;
        target.y = target.y - 1;
        if(this.isPiece(target)){
            text = text + " G ";
        }
        target.y = target.y + 2;
        if(this.isPiece(target)){
            text = text + " D ";
        }
        text = text + "\n";
        return text;
    }

    public void printMap(){ // for debug
        int i,j;
        for (i = 0; i < this.carte.length; i++) {
            System.out.print(i);
            for (j = 0; j < this.carte[i].length; j++) {
                if (this.carte[i][j] instanceof Combat){
                    System.out.print("C");
                }else{
                    if (this.carte[i][j] instanceof Boutique){
                        System.out.print("B");
                    }else{
                        if (this.carte[i][j] instanceof Piece){
                            System.out.print("P");
                        }else{
                            System.out.print("*");
                        }
                    }
                }
            }
            System.out.print("\n");
        }
    }
}
