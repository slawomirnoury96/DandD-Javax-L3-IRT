package main.EtreVivant;

import java.util.concurrent.ThreadLocalRandom;

public class EtreVivant {
    protected String nom;
    protected Integer pv;

    protected EtreVivant(String nom, Integer pv){ 
        this.nom=nom;
        this.pv=pv;
    }

    public String getNom() {
		return this.nom;
	}

    public Integer getPointDeVie() {
		return this.pv;
	}

    public Integer attaquer(){ 
        return ThreadLocalRandom.current().nextInt(0, 1 + 1);
    }

    public Integer subirAtk(Integer degat) {
        if(degat>0){
            this.pv = this.pv - degat;
            System.out.print(this.nom + " Subit une attaque de "+ degat +" , Nombre de PV restant" + this.pv + "\n");
        }
        //Possibilité d'ameliorataion Inserer une condition IF si le personnage meurt peut-étre
		return this.pv;
	}
}

