package main.EtreVivant;

import main.DataType.Point;
import main.Equipement.*;

public class Joueur extends EtreVivant {

    protected Armure armure;
    protected Arme arme;
    protected Potion potion;
    protected Integer argent;

    protected Equipement inventaire[];
    protected Integer inventaire_size;

    protected Point Position;

    /**Méthode Joueur où le joueur est attribué 10 sous et 10pdv */
    public Joueur(String nom){
        super(nom,3);
        this.argent = 10;
        this.inventaire_size = 10;
        this.inventaire = new Equipement[inventaire_size]; //Inventaire de 10
        this.Position = new Point();
    }

    /** @return On liste une chaine de caractère de la liste des équipements*/
    public String liste_equipement(){
        String text = this.nom + " porte / a ";

        if (armure == null){
            text = text + "aucune armure,";
        }else{
            text = text + "armure oui";
        }
        if (arme == null){
            text = text + "aucune arme,";
        }else{
            text = text + "arme de " + this.arme.getBonus() + " de dégat,";
        }
        if (potion == null){
            text = text + "aucune potion.\n";
        }
        return text;
    }

    /**Méthode d'attaque */
    public Integer attaquer(){
        Integer degat = super.attaquer();
        if((this.arme != null)&&(degat!=0)){
            degat = degat + this.arme.getBonus();
        }
        return degat;
    }
    
    /**Méthode subir attaque */
    public Integer subirAtk(Integer degat){
        if(this.armure != null){
            degat = degat - this.arme.getBonus();
        }
        if(degat>0){
            this.pv = this.pv - degat;
            System.out.print(this.nom + " Subit une attaque de "+ degat +" , Nombre de PV restant" + this.pv + "\n");
        }
        return this.pv;
    }

    /** Sets the user's position */
    public String setPosition(Point point){ 
        String text = this.nom +" à changer de piéce\n";
        this.Position.x = point.x;
        this.Position.y = point.y;
        return text;
    }

    /** Permet de choisir la position suivante ou next move et retourne un point ( x + y)  */
    public Point NextPosition(String direction){
        Point point = new Point();
        point.x = this.Position.x;
        point.y = this.Position.y;
        switch (direction) {
            case "H":  point.x = point.x - 1;
                     break;
            case "B":  point.x = point.x + 1;
                     break;
            case "G":  point.y = point.y - 1;
                     break;
            case "D":  point.y = point.y + 1;
                     break;
            default:    point = null;
        }
        return point;
    }

    public String getNom(){
        return this.nom;
    }

    public Point getPosition(){
        return this.Position;
    }

    public Boolean equiper(Integer selection){
        
        if (this.inventaire[selection]  instanceof Armure){
            this.armure = (Armure)this.inventaire[selection];
            return true;
        }else{
            if (this.inventaire[selection]  instanceof Arme){
                this.arme = (Arme)this.inventaire[selection];
                return true;
            }else{
                if(this.inventaire[selection]  instanceof Potion){
                    this.pv = this.pv + this.inventaire[selection].getBonus();
                    this.inventaire[selection] = null; //On détruit la potion
                }
            }
        }
        return false;
    }

    public void deequiper(){

    }

    public String getInventaire(){ 
        String text = "Piéce d'or : "+this.argent+"\nPoint de vie : "+ this.pv +"\nInventaire : \n";

        int i;
            for (i = 0; i < this.inventaire.length; i++) {
                if (this.inventaire[i] instanceof Potion){
                    text = text + "Potion Slot " + i + " Bonus :"+this.inventaire[i].getBonus()+"\n";
                }else{
                    if (this.inventaire[i]  instanceof Armure){
                        text = text + "Armure Slot " + i + " Bonus :"+this.inventaire[i].getBonus()+"\n";
                    }else{
                        if (this.inventaire[i]  instanceof Arme){
                            text = text + "Arme Slot " + i + " Bonus :"+this.inventaire[i].getBonus()+"\n";
                        }else{
                            //Code si slot vide
                        }
                    }
                }
        }

        return text;
    }

    public Boolean addInventaire(Equipement gear){
        Integer i;
        for(i = 0;i<inventaire_size;i++){
            if(this.inventaire[i] == null){
                this.inventaire[i]=gear;
                return true;
            }
        }

        return false;
    }

    public void addArgent(Integer gain){
        this.argent = this.argent + gain;
    }

    public Boolean removeArgent(Integer prix){ //retourne false si pas assez 
        if(this.argent>=prix){
            this.argent = this.argent - prix;
            System.out.print("He he he… Thank you!\n");
            return true;
        }else{
            System.out.print("Not enough cash, stranger!\n"); // Message d'erreur, pass assez d'argent, C'est cheap de le mettre ici
            return false;
        }
        
    }


    public Integer getArgent(){
        return this.argent;
    }


}
