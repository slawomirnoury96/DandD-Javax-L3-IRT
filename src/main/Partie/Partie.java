package main.Partie;


import main.DataType.Point;
import main.EtreVivant.Joueur;
//import main.EtreVivant.Monstre;
import main.Labyrinthe.Labyrinthe;
import main.Piece.*;
import main.Boundaries.Interaction;


public class Partie {
    public static void main(String[] args){
        Interaction user  = new Interaction();
        String input;
        Joueur joueur = new Joueur(user.getNom());
        System.out.print("Le Joueur "+ joueur.getNom() + " a été crée\n");
        Labyrinthe carte = new Labyrinthe();
        System.out.print("Labyrinthe à été crée\n");
        Point bufferPosition;
        Point lastPosition = new Point();
        joueur.setPosition(carte.getBoutiquePoint());
        System.out.print("Joueur déplacé\n");
        loop_game :
        while(true){

            if(carte.isBoutique(joueur.getPosition())){
                System.out.print("Piéce de boutique\nMenu Boutique disponible en choix 4\n");
            }

            if(carte.isCombat(joueur.getPosition())){
                
                System.out.print("Piéce de combat\n");
                Combat piece = carte.getCombat(joueur.getPosition());
                if(piece.IsMonstre()){ //On verifie si il y'a des monstes
                    switch(piece.InCombat(joueur)){
                        case "win" : 
                            System.out.println(joueur.getNom() + " a win !\n");
                            carte.setPiece(joueur.getPosition());// patch pour sortir de la boucle de combat si plus de monstre
                            break;
                        case "lose" : 
                            System.out.println("Le joueur est mort \n");
                            break loop_game;
                        default : 
                            String choix = user.choixCombat();
                            if(choix.equals("Y")||choix.equals("y")){
                                System.out.println("On quitte le combat \n"); //Code pour quiter le combat
                                joueur.setPosition(lastPosition);
                                System.out.println("Retour à l'ancienne pièce \n");
                            }
                            break;
                    }
                    
                }
            }else{
            //Affichage du menu géneral
            input = user.getAction();
            switch(input){
                case "1": //Déplacement pesonnage
                    //System.out.print(carte.getPorte(joueur.getPosition()));
                    input = user.getDoor(carte.getPorte(joueur.getPosition()));
                    if(carte.isPiece(joueur.NextPosition(input))){
                        bufferPosition = joueur.getPosition();
                        lastPosition.x = bufferPosition.x;
                        lastPosition.y = bufferPosition.y;
                        joueur.setPosition(joueur.NextPosition(input));
                        System.out.print("Mouvement fait\n");
                    }else{
                        System.out.print("Mouvement immposible\n");
                    }
                    break;
                case "2":
                    System.out.print(joueur.getInventaire());
                    break;
                case "3":
                    joueur.equiper(Integer.parseInt(user.getItem(joueur.getInventaire())));//PAS DE VERIF SUR INTEGER NOT GOOD, A FIX 
                    break;
                case "4":
                    if(carte.isBoutique(joueur.getPosition())){
                        Boutique boutique = carte.getBoutique(joueur.getPosition());
                        System.out.print("Weeeelllllcoooooommmmmeeee…\nWhat are you buyin?\n");
                        System.out.print(boutique.getInventaire());
                        boutique_loop:
                        while(true){
                            switch(user.choixBoutique()){
                                case "1":
                                    if(joueur.removeArgent(boutique.getPrix())){
                                        joueur.addInventaire(boutique.getArme());
                                    }
                                    break;
                                case "2":
                                    if(joueur.removeArgent(boutique.getPrix())){
                                        joueur.addInventaire(boutique.getArmure());
                                    }
                                    break;
                                case "3":
                                    if(joueur.removeArgent(boutique.getPrix())){
                                        joueur.addInventaire(boutique.getPotion());
                                    }
                                    break;
                                case "c":
                                    break boutique_loop;
                                default:
                                    System.out.print("I don't understands, sowry\n");
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    System.out.print("I don't understand, sowry\n");
                    break;
            }
            

            }

        }
    }
}
 




