package main.Boundaries;

import java.util.Scanner;

public class Interaction {
    

    public Interaction(){

    }



    public String getDoor(String Door){
        System.out.print(Door);
        System.out.print("Quel porte prendre ?\n");
        String input = this.input();
        return input.toUpperCase();
    }

    public String getAction(){
        System.out.print("\nQue faire ?\n\nBouger => 1\nRegarder inventaire => 2\nEquiper => 3\n");
        return this.input();
    }

    public String getItem(String Item_list){
        System.out.print(Item_list);
        System.out.print("Quel item utiliser?\n");
        return this.input();
    }

    public String getNom(){
        while(true){
            System.out.print("Quelle est le nom du personage ?\n");
            String nom = this.input();
            if(nom.matches("^[a-zA-Z]*$")){ //On verifie que le nom contient des caractére humain
                return nom;
            }
            System.out.print("Input faux\n");
        }
        
        
    }

    public String choixCombat(){
        while(true){
            System.out.print("Voulez vous fuir ? Y N\n");
            String choix = this.input();
            if(choix.equals("Y")||choix.equals("y")||choix.equals("N")||choix.equals("n")){
                return choix;
            }
            System.out.print("Input faux\n");
        }
        
    }

    public String choixBoutique(){
        while(true){
            System.out.print("Quel item voulez vous acheter ? Arme => 1 ,Armure => 2 et Potion => 3, C pour quiter\n");
            String choix = this.input();
            if(choix.equals("C")){
                choix = "c";// pour eviter les multiple case dans le "main"
            }
            if(choix.equals("1")||choix.equals("2")||choix.equals("3")||choix.equals("c")){
                return choix;
            }
            System.out.print("Input faux\n");
        }
        
    }

    protected String input(){ //Méthode pour récupérer l'input user
        Scanner scan = new Scanner(System.in); 
        return scan.nextLine();
    }

}
