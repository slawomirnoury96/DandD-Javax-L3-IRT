package test;

import main.EtreVivant.*;
import main.Piece.Combat;

import java.util.Scanner;

import main.Equipement.*;


public class TestCombat{
    
    public static void main(String[] args) {
        Joueur pelo = new Joueur("Philipe");
        System.out.print(pelo.getNom()+" a "+pelo.getPointDeVie()+" Point de Vie\n");
        Combat piece = new Combat();
        Monstre groupe[] = piece.GetMonstre();
        //Sauvgarder la position de joueur avant de renter dans le combat pour la fonction fuir
        while(true){
            //Pour li'tégration multi-monstre, ajourter des véfication null sur les monstres mort avant l'attaque
            groupe[0].subirAtk(pelo.attaquer());
            if(groupe[0].getPointDeVie()<=0){ //On check si le monstre est mort, si oui code en dessous
                groupe[0] = null;
                if(piece.IsMonstre()){
                    //On a encore des monstre
                }else{
                    System.out.println("Les monstres sont morts \n");
                    break;
                }
                
            }
            pelo.subirAtk(groupe[0].attaquer());
            if(pelo.getPointDeVie()<=0){ //On check si le joueur est mort, si oui code en dessous
                System.out.println("Le joueur est mort \n");
                break;
            }

            Scanner input = new Scanner(System.in);
            System.out.println("Voulez vous fuir ? Y N\n");
            String choix = input.nextLine();
            if(choix.equals("Y")||choix.equals("y")){
                System.out.println("On quitte le combat \n"); //Code pour quiter le combat
                break;
            }

        }
    }
}
